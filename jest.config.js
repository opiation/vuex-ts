module.exports = {
  collectCoverage: true,
  coverageReporters: ["text"],
  globals: {
    "ts-jest": {
      tsConfig: {
        esModuleInterop: true
      }
    }
  },
  preset: "ts-jest",
  reporters: [
    "default",
    [
      "jest-junit",
      {
        outputDirectory: "./coverage",
        outputName: "./junit.xml",
        suiteName: "Jest tests"
      }
    ]
  ],
  testEnvironment: "node",
  verbose: true
}
