import * as Vuex from "../src"

describe("index.ts", () => {
  it("exports a default a non-empty object", () => {
    expect(Vuex).toBeDefined()
  })
})
