import Vue from "vue"
import { Dispatch } from "./actions"
import { Commit } from "./mutations"
import { Store } from "./store"

interface Dictionary<T> {
  [key: string]: T
}
type Computed = () => any
type MutationMethod = (...args: any[]) => void
type ActionMethod = (...args: any[]) => Promise<any>
type CustomVue = Vue & Dictionary<any>

type Mapper<R> = (map: string[] | Dictionary<string>) => Dictionary<R>
type MapperWithNamespace<R> = (
  namespace: string,
  map: string[] | Dictionary<string>
) => Dictionary<R>

type FunctionMapper<F, R> = (
  map: Dictionary<(this: CustomVue, fn: F, ...args: any[]) => any>
) => Dictionary<R>
type FunctionMapperWithNamespace<F, R> = (
  namespace: string,
  map: Dictionary<(this: CustomVue, fn: F, ...args: any[]) => any>
) => Dictionary<R>

type MapperForState = <S>(
  map: Dictionary<(this: CustomVue, state: S, getters: any) => any>
) => Dictionary<Computed>
type MapperForStateWithNamespace = <S>(
  namespace: string,
  map: Dictionary<(this: CustomVue, state: S, getters: any) => any>
) => Dictionary<Computed>

interface NamespacedMappers {
  mapState: Mapper<Computed> & MapperForState
  mapMutations: Mapper<MutationMethod> & FunctionMapper<Commit, MutationMethod>
  mapGetters: Mapper<Computed>
  mapActions: Mapper<ActionMethod> & FunctionMapper<Dispatch, ActionMethod>
}

type StateMapper = Mapper<Computed> &
  MapperWithNamespace<Computed> &
  MapperForState &
  MapperForStateWithNamespace
/**
 * Reduce the code which written in Vue.js for getting the state.
 * @param [namespace] Module's namespace
 * @param states Object's item can be a function which accept state and getters
 *   for param, you can do something for state and getters in it.
 */
export const mapState: StateMapper = normalizeNamespace(
  (namespace: string, states: any | any[]) => {
    const res: any = {}

    normalizeMap(states).forEach(({ key, val }) => {
      res[key] = function mappedState() {
        let state = this.$store.state
        let getters = this.$store.getters
        if (namespace) {
          const module = getModuleByNamespace(
            this.$store,
            "mapState",
            namespace
          )
          if (!module) {
            return
          }
          state = module.context.state
          getters = module.context.getters
        }
        return typeof val === "function"
          ? val.call(this, state, getters)
          : state[val]
      }
      // mark vuex getter for devtools
      res[key].vuex = true
    })

    return res
  }
)

type MutationMapper = Mapper<MutationMethod> &
  MapperWithNamespace<MutationMethod> &
  FunctionMapper<Commit, MutationMethod> &
  FunctionMapperWithNamespace<Commit, MutationMethod>
/**
 * Reduce the code which written in Vue.js for committing the mutation
 * @param [namespace] Module's namespace
 * @param mutations Object's item can be a function which accept `commit`
 *   function as the first param, it can accept anthor params.  You can commit
 *   mutation and do any other things in this function.  Specially, you need to
 *   pass anthor params from the mapped function.
 * @return {Object}
 */
export const mapMutations: MutationMapper = normalizeNamespace(
  (namespace, mutations) => {
    const res: any = {}

    normalizeMap(mutations).forEach(({ key, val }) => {
      res[key] = function mappedMutation(...args: any[]) {
        // Get the commit method from store
        let commit = this.$store.commit
        if (namespace) {
          const module = getModuleByNamespace(
            this.$store,
            "mapMutations",
            namespace
          )
          if (!module) {
            return
          }
          commit = module.context.commit
        }
        return typeof val === "function"
          ? val.apply(this, [commit].concat(args))
          : commit.apply(this.$store, [val].concat(args))
      }
    })

    return res
  }
)

type GetterMapper = Mapper<Computed> & MapperWithNamespace<Computed>
/**
 * Reduce the code which written in Vue.js for getting the getters
 * @param [namespace] Module's namespace
 * @param getters
 */
export const mapGetters: GetterMapper = normalizeNamespace(
  (namespace, getters) => {
    const res: any = {}

    normalizeMap(getters).forEach(({ key, val }) => {
      // The namespace has been mutated by normalizeNamespace
      val = namespace + val
      res[key] = function mappedGetter() {
        if (
          namespace &&
          !getModuleByNamespace(this.$store, "mapGetters", namespace)
        ) {
          return
        }
        if (
          process.env.NODE_ENV !== "production" &&
          !(val in this.$store.getters)
        ) {
          console.error(`[vuex] unknown getter: ${val}`)
          return
        }
        return this.$store.getters[val]
      }
      // mark vuex getter for devtools
      res[key].vuex = true
    })
    return res
  }
)

type ActionMapper = Mapper<ActionMethod> &
  MapperWithNamespace<ActionMethod> &
  FunctionMapper<Dispatch, ActionMethod> &
  FunctionMapperWithNamespace<Dispatch, ActionMethod>
/**
 * Reduce the code which written in Vue.js for dispatch the action
 * @param [namespace] Module's namespace
 * @param actions Object's item can be a function which accept `dispatch`
 *   function as the first param, it can accept anthor params. You can dispatch
 *   action and do any other things in this function. Specially, tou need to
 *   pass anthor params from the mapped function.
 * @return
 */
export const mapActions: ActionMapper = normalizeNamespace(
  (namespace, actions) => {
    const res: any = {}

    normalizeMap(actions).forEach(({ key, val }) => {
      res[key] = function mappedAction(...args: any[]) {
        // get dispatch function from store
        let dispatch = this.$store.dispatch
        if (namespace) {
          const module = getModuleByNamespace(
            this.$store,
            "mapActions",
            namespace
          )
          if (!module) {
            return
          }
          dispatch = module.context.dispatch
        }
        return typeof val === "function"
          ? val.apply(this, [dispatch].concat(args))
          : dispatch.apply(this.$store, [val].concat(args))
      }
    })
    return res
  }
)

/**
 * Rebinding namespace param for mapXXX function in special scoped, and return
 * them by simple object
 * @param namespace
 * @todo Should return object of type `NamespacedMappers`
 */
export const createNamespacedHelpers = (namespace: string) => ({
  mapActions: mapActions.bind(null, namespace),
  mapGetters: mapGetters.bind(null, namespace),
  mapMutations: mapMutations.bind(null, namespace),
  mapState: mapState.bind(null, namespace)
})

/**
 * Normalize the map.  If given an array of values, each value will become an
 * object whose `key` is the array value and `val` is also the array value.  If
 * the map is an object, each key-value pair will become an object whose `key`
 * is the key and whose `val` is the value.
 *
 * @example
 * normalizeMap([1, 2, 3])
 * // => [ { key: 1, val: 1 }, { key: 2, val: 2 }, { key: 3, val: 3 } ]
 *
 * @example
 * normalizeMap({a: 1, b: 2, c: 3})
 * // => [ { key: 'a', val: 1 }, { key: 'b', val: 2 }, { key: 'c', val: 3 } ]
 * @param map
 */
function normalizeMap<T = any>(map: T[]): Array<{ key: T; val: T }>
function normalizeMap<T = object>(map: T): Array<{ key: keyof T; val: any }> {
  if (Array.isArray(map)) {
    return map.map(key => ({ key, val: key }))
  }

  const keys = Object.keys(map) as Array<keyof T>
  return keys.map(key => ({ key, val: map[key] }))
}

/**
 * Return a function expect two param contains namespace and map.  It will
 * normalize the namespace and then the param's function will handle the new
 * namespace and the map.
 * @param fn
 */
function normalizeNamespace<T = any, R = any>(
  fn: (namespace: string, map: T) => R
) {
  function normalized(namespace: any): R
  function normalized(namespace: string, map?: T): R {
    if (typeof namespace !== "string") {
      map = namespace
      namespace = ""
    } else if (namespace.charAt(namespace.length - 1) !== "/") {
      namespace += "/"
    }

    if (map === undefined) {
      throw new TypeError(`map must be a valid type`)
    }

    return fn(namespace, map)
  }

  return normalized
}

/**
 * Search a special module from store by namespace. if module not exist, print
 * error message.
 * @param store
 * @param helper
 * @param namespace
 */
function getModuleByNamespace<T = any>(
  store: Store<T>,
  helper: string,
  namespace: string
) {
  const module = store._modulesNamespaceMap[namespace]

  if (process.env.NODE_ENV !== "production" && !module) {
    console.error(
      `[vuex] module namespace not found in ${helper}(): ${namespace}`
    )
  }

  return module
}
