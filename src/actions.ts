import { Commit } from "./mutations"
import { Payload } from "./payloads"
import { Store } from "./store"

export type Action<S, R> = ActionHandler<S, R> | ActionObject<S, R>

export interface ActionTree<S, R> {
  [key: string]: Action<S, R>
}

export interface ActionContext<S, R> {
  dispatch: Dispatch
  commit: Commit
  state: S
  getters: any
  rootState: R
  rootGetters: any
}

export type ActionHandler<S, R> = (
  this: Store<R>,
  injectee: ActionContext<S, R>,
  payload: any
) => any

export interface ActionObject<S, R> {
  root?: boolean
  handler: ActionHandler<S, R>
}

export interface ActionPayload extends Payload {
  payload: any
}

export type ActionSubscriber<P, S> = (action: P, state: S) => any

export interface ActionSubscribersObject<P, S> {
  before?: ActionSubscriber<P, S>
  after?: ActionSubscriber<P, S>
}

export interface Dispatch {
  (type: string, payload?: any, options?: DispatchOptions): Promise<any>
  <P extends Payload>(payloadWithType: P, options?: DispatchOptions): Promise<
    any
  >
}

export interface DispatchOptions {
  root?: boolean
}

export type SubscribeActionOptions<P, S> =
  | ActionSubscriber<P, S>
  | ActionSubscribersObject<P, S>
