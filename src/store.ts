import _Vue, { VueConstructor, WatchOptions } from "vue"

import {
  ActionPayload,
  ActionTree,
  DispatchOptions,
  SubscribeActionOptions
} from "./actions"
import { GetterTree } from "./getters"
import { mixin as applyMixin } from "./mixin"
import { Module, ModuleOptions, ModuleTree } from "./module/module"
import ModuleCollection from "./module/module-collection"
import { CommitOptions, MutationPayload, MutationTree } from "./mutations"
import { Payload } from "./payloads"
import devtoolPlugin from "./plugins/devtool"
import { assert, forEachValue, isObject, isPromise } from "./util"

let Vue: any // bind on install

declare module "vue/types/options" {
  interface ComponentOptions<V extends _Vue> {
    store?: Store<any>
  }
}

declare module "vue/types/vue" {
  interface Vue {
    $store: Store<any>
  }
}

export type Plugin<S> = (store: Store<S>) => any

// tslint:disable:variable-name
export class Store<RootState> {
  public _actions: any
  public _actionSubscribers: any
  public _committing: any
  public _devtoolHook: any
  public _modules: ModuleCollection<any>
  public _modulesNamespaceMap: any
  public _mutations: any
  public _subscribers: any
  public _vm: any
  public _watcherVM: any
  public _wrappedGetters: any
  public getters: any
  public strict: boolean

  constructor(options: StoreOptions<RootState> = {}) {
    // Auto install if it is not done yet and `window` has `Vue`.
    // To allow users to avoid auto-installation in some cases,
    // this code should be placed here. See #731
    if (!Vue && typeof window !== "undefined" && window.Vue) {
      install(window.Vue as any)
    }

    if (process.env.NODE_ENV !== "production") {
      assert(Vue, `must call Vue.use(Vuex) before creating a store instance.`)
      assert(
        typeof Promise !== "undefined",
        `vuex requires a Promise polyfill in this browser.`
      )
      assert(
        this instanceof Store,
        `store must be called with the new operator.`
      )
    }

    const { plugins = [], strict = false } = options

    // store internal state
    this._committing = false
    this._actions = Object.create(null)
    this._actionSubscribers = []
    this._mutations = Object.create(null)
    this._wrappedGetters = Object.create(null)
    this._modules = new ModuleCollection(options)
    this._modulesNamespaceMap = Object.create(null)
    this._subscribers = []
    this._watcherVM = new Vue()

    // bind commit and dispatch to self
    // tslint:disable-next-line:no-this-assignment
    const store = this
    // tslint:disable-next-line:no-this-assignment
    const { dispatch, commit } = this

    this.dispatch = function boundDispatch(type, payload) {
      return dispatch.call(store, type, payload)
    }
    this.commit = function boundCommit(type, payload, commitOptions) {
      return commit.call(store, type, payload, commitOptions)
    }

    // strict mode
    this.strict = strict

    const state = this._modules.root.state

    // init root module.
    // this also recursively registers all sub-modules
    // and collects all module getters inside this._wrappedGetters
    installModule(this, state, [], this._modules.root)

    // initialize the store vm, which is responsible for the reactivity
    // (also registers _wrappedGetters as computed properties)
    resetStoreVM(this, state)

    // apply plugins
    plugins.forEach((plugin: (store: Store<any>) => void) => plugin(this))

    const useDevtools =
      options.devtools !== undefined ? options.devtools : Vue.config.devtools
    if (useDevtools) {
      devtoolPlugin(this)
    }
  }

  get state(): RootState {
    return this._vm._data.$$state
  }

  set state(v: RootState) {
    if (process.env.NODE_ENV !== "production") {
      assert(false, `use store.replaceState() to explicit replace store state.`)
    }
  }

  // Should be of type `Commit`
  public commit(_type: string, _payload?: any, _options?: CommitOptions): void
  public commit<P extends Payload>(
    _type: P,
    _payload?: CommitOptions,
    _options?: any
  ): void {
    // check object-style commit
    const { type, payload, options } = unifyObjectStyle(
      _type,
      _payload,
      _options
    )

    const mutation = { type, payload }
    const entry = this._mutations[type]
    if (!entry) {
      if (process.env.NODE_ENV !== "production") {
        console.error(`[vuex] unknown mutation type: ${type}`)
      }
      return
    }
    this._withCommit(() => {
      entry.forEach(function commitIterator(handler: any) {
        handler(payload)
      })
    })
    this._subscribers.forEach((sub: any) => sub(mutation, this.state))

    if (process.env.NODE_ENV !== "production" && options && options.silent) {
      console.warn(
        `[vuex] mutation type: ${type}. Silent option has been removed. ` +
          "Use the filter functionality in the vue-devtools"
      )
    }
  }

  // Of type `Dispatch`
  public dispatch(
    _type: string,
    payload?: any,
    options?: DispatchOptions
  ): Promise<any>
  public dispatch<P extends Payload>(
    _type: P,
    _payload?: DispatchOptions
  ): Promise<any> {
    // check object-style dispatch
    const { type, payload } = unifyObjectStyle(_type, _payload)

    const action = { type, payload }
    const entry = this._actions[type]
    if (!entry) {
      if (process.env.NODE_ENV !== "production") {
        console.error(`[vuex] unknown action type: ${type}`)
      }
      return Promise.reject()
    }

    try {
      this._actionSubscribers
        .filter((sub: any) => sub.before)
        .forEach((sub: any) => sub.before(action, this.state))
    } catch (e) {
      if (process.env.NODE_ENV !== "production") {
        console.warn(`[vuex] error in before action subscribers: `)
        console.error(e)
      }
    }

    const result =
      entry.length > 1
        ? Promise.all(entry.map((handler: any) => handler(payload)))
        : entry[0](payload)

    return result.then((res: any) => {
      try {
        this._actionSubscribers
          .filter((sub: any) => sub.after)
          .forEach((sub: any) => sub.after(action, this.state))
      } catch (e) {
        if (process.env.NODE_ENV !== "production") {
          console.warn(`[vuex] error in after action subscribers: `)
          console.error(e)
        }
      }
      return res
    })
  }

  public subscribe<P extends MutationPayload>(
    fn: (mutation: P, state: RootState) => any
  ) {
    return genericSubscribe(fn, this._subscribers)
  }

  public subscribeAction<P extends ActionPayload>(
    fn: SubscribeActionOptions<P, RootState>
  ) {
    const subs = typeof fn === "function" ? { before: fn } : fn
    return genericSubscribe(subs, this._actionSubscribers)
  }

  public watch<T>(
    getter: (state: RootState, getters: any) => T,
    cb: (value: T, oldValue: T) => void,
    options?: WatchOptions
  ) {
    if (process.env.NODE_ENV !== "production") {
      assert(
        typeof getter === "function",
        `store.watch only accepts a function.`
      )
    }
    return this._watcherVM.$watch(
      () => getter(this.state, this.getters),
      cb,
      options
    )
  }

  public replaceState(state: RootState) {
    this._withCommit(() => {
      this._vm._data.$$state = state
    })
  }

  public registerModule<ModuleState>(
    path: string | string[],
    rawModule: Module<ModuleState, RootState>,
    options: ModuleOptions = {}
  ) {
    if (typeof path === "string") {
      path = [path]
    }

    if (process.env.NODE_ENV !== "production") {
      assert(Array.isArray(path), `module path must be a string or an Array.`)
      assert(
        path.length > 0,
        "cannot register the root module by using registerModule."
      )
    }

    this._modules.register(path, rawModule)
    installModule(
      this,
      this.state,
      path,
      this._modules.get(path),
      options.preserveState
    )
    // reset store to update getters...
    resetStoreVM(this, this.state)
  }

  public unregisterModule(path: string | string[]) {
    const normalizedPath = typeof path === "string" ? [path] : path

    if (process.env.NODE_ENV !== "production") {
      assert(
        Array.isArray(normalizedPath),
        `module path must be a string or an Array.`
      )
    }

    this._modules.unregister(normalizedPath)
    this._withCommit(() => {
      const parentState = getNestedState(
        this.state,
        normalizedPath.slice(0, -1)
      )
      Vue.delete(parentState, normalizedPath[normalizedPath.length - 1])
    })
    resetStore(this)
  }

  public hotUpdate(newOptions: {
    actions?: ActionTree<RootState, RootState>
    getters?: GetterTree<RootState, RootState>
    modules?: ModuleTree<RootState>
    mutations?: MutationTree<RootState>
  }) {
    this._modules.update(newOptions)
    resetStore(this, true)
  }

  public _withCommit(fn: any) {
    const committing = this._committing
    this._committing = true
    fn()
    this._committing = committing
  }
}

export interface StoreOptions<S> {
  state?: S | (() => S)
  getters?: GetterTree<S, S>
  actions?: ActionTree<S, S>
  mutations?: MutationTree<S>
  modules?: any // ModuleTree<S>
  plugins?: Array<Plugin<S>>
  strict?: boolean

  devtools?: any
}

function genericSubscribe(fn: any, subs: any) {
  if (subs.indexOf(fn) < 0) {
    subs.push(fn)
  }
  return () => {
    const i = subs.indexOf(fn)
    if (i > -1) {
      subs.splice(i, 1)
    }
  }
}

function resetStore<T = any>(store: Store<T>, hot: boolean = false) {
  store._actions = Object.create(null)
  store._mutations = Object.create(null)
  store._wrappedGetters = Object.create(null)
  store._modulesNamespaceMap = Object.create(null)
  const state = store.state
  // init all modules
  installModule(store, state, [], store._modules.root, true)
  // reset vm
  resetStoreVM(store, state, hot)
}

function resetStoreVM<T = any>(
  store: Store<T>,
  state: any,
  hot: boolean = false
) {
  const oldVm = store._vm

  // bind store public getters
  store.getters = {}
  const wrappedGetters = store._wrappedGetters
  const computed: any = {}
  forEachValue(wrappedGetters, (fn, key) => {
    // use computed to leverage its lazy-caching mechanism
    computed[key] = () => fn(store)
    Object.defineProperty(store.getters, key, {
      enumerable: true, // for local getters
      get: () => store._vm[key]
    })
  })

  // use a Vue instance to store the state tree
  // suppress warnings just in case the user has added
  // some funky global mixins
  const silent = Vue.config.silent
  Vue.config.silent = true
  store._vm = new Vue({
    computed,
    data: {
      $$state: state
    }
  })
  Vue.config.silent = silent

  // enable strict mode for new vm
  if (store.strict) {
    enableStrictMode(store)
  }

  if (oldVm) {
    if (hot) {
      // dispatch changes in all subscribed watchers
      // to force getter re-evaluation for hot reloading.
      store._withCommit(() => {
        oldVm._data.$$state = null
      })
    }
    Vue.nextTick(() => oldVm.$destroy())
  }
}

function installModule<RootState>(
  store: Store<RootState>,
  rootState: RootState,
  path: any,
  module: any,
  hot: boolean = false
) {
  const isRoot = !path.length
  const namespace = store._modules.getNamespace(path)

  // register in namespace map
  if (module.namespaced) {
    store._modulesNamespaceMap[namespace] = module
  }

  // set state
  if (!isRoot && !hot) {
    const parentState = getNestedState(rootState, path.slice(0, -1))
    const moduleName = path[path.length - 1]
    store._withCommit(() => {
      Vue.set(parentState, moduleName, module.state)
    })
  }

  const local = (module.context = makeLocalContext(store, namespace, path))

  module.forEachMutation((mutation: any, key: any) => {
    const namespacedType = namespace + key
    registerMutation(store, namespacedType, mutation, local)
  })

  module.forEachAction((action: any, key: any) => {
    const type = action.root ? key : namespace + key
    const handler = action.handler || action
    registerAction(store, type, handler, local)
  })

  module.forEachGetter((getter: any, key: any) => {
    const namespacedType = namespace + key
    registerGetter(store, namespacedType, getter, local)
  })

  module.forEachChild((child: any, key: string) => {
    installModule(store, rootState, path.concat(key), child, hot)
  })
}

/**
 * make localized dispatch, commit, getters and state
 * if there is no namespace, just use root ones
 */
function makeLocalContext<RootState>(
  store: Store<RootState>,
  namespace: string,
  path: any
) {
  const noNamespace = namespace === ""

  const local = {
    dispatch: noNamespace
      ? store.dispatch
      : (_type: any, _payload: any, _options: any) => {
          const args = unifyObjectStyle(_type, _payload, _options)
          const { payload, options } = args
          let { type } = args

          if (!options || !options.root) {
            type = namespace + type
            if (
              process.env.NODE_ENV !== "production" &&
              !store._actions[type]
            ) {
              console.error(
                `[vuex] unknown local action type: ${
                  args.type
                }, global type: ${type}`
              )
              return
            }
          }

          return store.dispatch(type, payload)
        },

    commit: noNamespace
      ? store.commit
      : (_type: any, _payload: any, _options: any) => {
          const args = unifyObjectStyle(_type, _payload, _options)
          const { payload, options } = args
          let { type } = args

          if (!options || !options.root) {
            type = namespace + type
            if (
              process.env.NODE_ENV !== "production" &&
              !store._mutations[type]
            ) {
              console.error(
                `[vuex] unknown local mutation type: ${
                  args.type
                }, global type: ${type}`
              )
              return
            }
          }

          store.commit(type, payload, options)
        }
  }

  // getters and state object must be gotten lazily
  // because they will be changed by vm update
  Object.defineProperties(local, {
    getters: {
      get: noNamespace
        ? () => store.getters
        : () => makeLocalGetters(store, namespace)
    },
    state: {
      get: () => getNestedState(store.state, path)
    }
  })

  return local
}
// tslint:enable:variable-name

function makeLocalGetters<T = any>(store: Store<T>, namespace: string) {
  const gettersProxy = {}

  const splitPos = namespace.length
  Object.keys(store.getters).forEach(type => {
    // skip if the target getter is not match this namespace
    if (type.slice(0, splitPos) !== namespace) {
      return
    }

    // extract local getter type
    const localType = type.slice(splitPos)

    // Add a port to the getters proxy.
    // Define as getter property because
    // we do not want to evaluate the getters in this time.
    Object.defineProperty(gettersProxy, localType, {
      enumerable: true,
      get: () => store.getters[type]
    })
  })

  return gettersProxy
}

function registerMutation<T = any>(
  store: Store<T>,
  type: string,
  handler: any,
  local: any
) {
  const entry = store._mutations[type] || (store._mutations[type] = [])
  entry.push(function wrappedMutationHandler(payload: any) {
    handler.call(store, local.state, payload)
  })
}

function registerAction<T = any>(
  store: Store<T>,
  type: string,
  handler: any,
  local: any
) {
  const entry = store._actions[type] || (store._actions[type] = [])
  entry.push(function wrappedActionHandler(payload: any, cb: any) {
    let res = handler.call(
      store,
      {
        commit: local.commit,
        dispatch: local.dispatch,
        getters: local.getters,
        rootGetters: store.getters,
        rootState: store.state,
        state: local.state
      },
      payload,
      cb
    )
    if (!isPromise(res)) {
      res = Promise.resolve(res)
    }
    if (store._devtoolHook) {
      return res.catch((err: any) => {
        store._devtoolHook.emit("vuex:error", err)
        throw err
      })
    } else {
      return res
    }
  })
}

function registerGetter<T = any>(
  store: Store<T>,
  type: string,
  rawGetter: any,
  local: any
) {
  if (store._wrappedGetters[type]) {
    if (process.env.NODE_ENV !== "production") {
      console.error(`[vuex] duplicate getter key: ${type}`)
    }
    return
  }
  store._wrappedGetters[type] = function wrappedGetter(rootStore: Store<T>) {
    return rawGetter(
      local.state, // local state
      local.getters, // local getters
      rootStore.state, // root state
      rootStore.getters // root getters
    )
  }
}

function enableStrictMode<T = any>(store: Store<T>) {
  store._vm.$watch(
    function(this: typeof store._vm) {
      return this._data.$$state
    },
    () => {
      if (process.env.NODE_ENV !== "production") {
        assert(
          store._committing,
          `do not mutate vuex store state outside mutation handlers.`
        )
      }
    },
    { deep: true, sync: true }
  )
}

function getNestedState<State>(state: State, path: string[]) {
  return path.length
    ? path.reduce(
        (stateBranchOrLeaf: any, key: string) => stateBranchOrLeaf[key],
        state
      )
    : state
}

// TODO: Possible replace type with union of action interface and string
function unifyObjectStyle(type: any, payload: any, options?: any) {
  if (isObject(type) && type.type) {
    options = payload
    payload = type
    type = type.type
  }

  if (process.env.NODE_ENV !== "production") {
    assert(
      typeof type === "string",
      `expects string as the type, but found ${typeof type}.`
    )
  }

  return { type, payload, options }
}

// tslint:disable-next-line:variable-name
export function install(VueLocal: VueConstructor<_Vue>) {
  if (Vue && VueLocal === Vue) {
    if (process.env.NODE_ENV !== "production") {
      console.error(
        "[vuex] already installed. Vue.use(Vuex) should be called only once."
      )
    }
    return
  }
  Vue = VueLocal
  applyMixin(Vue)
}
