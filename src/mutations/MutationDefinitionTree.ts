import { MutationDefinition } from "./index"

export interface MutationDefinitionTree<State> {
  [type: string]: MutationDefinition<State>
}
