import { MutationDefinition } from "./index"

export type MutationDefinitionMap<Definitions> = {
  [Type in keyof Definitions]: Definitions[Type]
}

type MutationDefinitionPayloadType<
  MD extends MutationDefinition<any, any>
> = MD extends MutationDefinition<any, infer Payload> ? Payload : never

type CommiterMap<
  Definitions extends { [k: string]: MutationDefinition<any> }
> = {
  [Type in keyof Definitions]: (
    payload: MutationDefinitionPayloadType<Definitions[Type]>
  ) => void
}

interface State {
  age: number
}

const mutations = {
  one: (state: State, payload: number) => (state.age = payload)
}

const createCommiters = <T extends { [k: string]: MutationDefinition<any> }>(
  map: MutationDefinitionMap<T>
): CommiterMap<T> => {
  // Register mutations and unique mutators
  return {} as CommiterMap<T>
}

const commiters = createCommiters(mutations)
