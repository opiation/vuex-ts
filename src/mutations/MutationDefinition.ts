export interface MutationDefinition<State, Payload = any> {
  (state: State, payload: Payload): any
}
