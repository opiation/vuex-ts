import Vue, { VueConstructor } from "vue"

export const mixin = (V: VueConstructor) => {
  const version = Number(V.version.split(".")[0])

  if (version >= 2) {
    V.mixin({ beforeCreate: vuexInit })
  } else {
    throw new Error("Unsupported version of Vue. Must use >= 2.0")
  }

  /**
   * Vuex init hook, injected into each instances init hooks list.
   */
  function vuexInit(this: Vue) {
    const options = this.$options

    // store injection
    if (options.store) {
      this.$store =
        typeof options.store === "function"
          ? (options.store as any)()
          : options.store
    } else if (options.parent && options.parent.$store) {
      this.$store = options.parent.$store
    }
  }
}
