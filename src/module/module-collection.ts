import { StoreOptions } from "../store"
import { assert, forEachValue } from "../util"
import { Module, RawModule } from "./module"

export default class ModuleCollection<RootState> {
  public root: Module<RootState, RootState>

  constructor(rawRootModule: StoreOptions<RootState>) {
    // register root module (Vuex.Store options)
    if (process.env.NODE_ENV !== "production") {
      assertRawModule([], rawRootModule)
    }

    const newModule = new Module(rawRootModule, false)
    this.root = newModule
  }

  public get(path: string[]) {
    return path.reduce((module: Module<any, RootState>, key) => {
      return module.getChild(key)
    }, this.root)
  }

  public getNamespace(path: string[]) {
    let module = this.root
    return path.reduce((namespace, key) => {
      module = module.getChild(key)
      return namespace + (module.namespaced ? `${key}/` : "")
    }, "")
  }

  public update(rawRootModule: RawModule<RootState, RootState>) {
    update([], this.root, rawRootModule)
  }

  public register<ModuleState>(
    path: string[],
    rawModule: RawModule<ModuleState, RootState>,
    runtime = true
  ) {
    if (process.env.NODE_ENV !== "production") {
      assertRawModule(path, rawModule)
    }

    const newModule = new Module(rawModule, runtime)
    if (path.length === 0) {
      // If path is [], newModule is the root module of type Module<R, R>
      this.root = (newModule as unknown) as Module<RootState, RootState>
    } else {
      const parent = this.get(path.slice(0, -1))
      parent.addChild(path[path.length - 1], newModule)
    }

    // register nested modules
    if (rawModule.modules) {
      forEachValue(rawModule.modules, (rawChildModule, key) => {
        this.register(path.concat(key), rawChildModule, runtime)
      })
    }
  }

  public unregister(path: string[]) {
    const parent = this.get(path.slice(0, -1))
    const key = path[path.length - 1]
    if (!parent.getChild(key).runtime) {
      return
    }

    parent.removeChild(key)
  }
}

function update<ModuleState, RootState>(
  path: string[],
  targetModule: Module<ModuleState, RootState>,
  newModule: RawModule<ModuleState, RootState>
) {
  if (process.env.NODE_ENV !== "production") {
    assertRawModule(path, newModule)
  }

  // update target module
  targetModule.update(newModule)

  // update nested modules
  if (newModule.modules) {
    for (const key in newModule.modules) {
      if (newModule.modules.hasOwnProperty(key)) {
        if (!targetModule.getChild(key)) {
          if (process.env.NODE_ENV !== "production") {
            console.warn(
              `[vuex] trying to add a new module '${key}' on hot reloading, ` +
                "manual reload is needed"
            )
          }
          return
        }
        update(
          path.concat(key),
          targetModule.getChild(key),
          newModule.modules[key]
        )
      }
    }
  }
}

const functionAssert = {
  assert: (value: any) => typeof value === "function",
  expected: "function"
}

const objectAssert = {
  assert: (value: any) =>
    typeof value === "function" ||
    (typeof value === "object" && typeof value.handler === "function"),
  expected: 'function or object with "handler" function'
}

const assertTypes = {
  actions: objectAssert,
  getters: functionAssert,
  mutations: functionAssert
}

function assertRawModule<ModuleState, RootState>(
  path: string[],
  rawModule: RawModule<ModuleState, RootState>
) {
  const keys = Object.keys(assertTypes) as Array<keyof typeof assertTypes>

  keys.forEach(key => {
    if (!rawModule[key]) {
      return
    }

    const assertOptions = assertTypes[key]

    forEachValue(rawModule[key], (value, type) => {
      assert(
        assertOptions.assert(value),
        makeAssertionMessage(path, key, type, value, assertOptions.expected)
      )
    })
  })
}

function makeAssertionMessage(
  path: any,
  key: string,
  type: string,
  value: any,
  expected: string
) {
  let buf = `${key} should be ${expected} but "${key}.${type}"`
  if (path.length > 0) {
    buf += ` in module "${path.join(".")}"`
  }
  buf += ` is ${JSON.stringify(value)}.`
  return buf
}
