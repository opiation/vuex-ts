import { ActionTree } from "../actions"
import { GetterTree } from "../getters"
import { MutationTree } from "../mutations"
import { forEachValue } from "../util"

// Base data struct for store's module, package with some attribute and method
export class Module<ModuleState, RootState> {
  public runtime: boolean
  public state?: ModuleState

  public actions?: ActionTree<ModuleState, RootState>
  public getters?: GetterTree<ModuleState, RootState>
  public mutations?: MutationTree<ModuleState>

  // tslint:disable:variable-name
  private _children: { [key: string]: Module<any, RootState> }
  private _rawModule: RawModule<ModuleState, RootState>
  // tslint:enable:variable-name

  constructor(rawModule: RawModule<ModuleState, RootState>, runtime: boolean) {
    this.runtime = runtime
    // Store some children item
    this._children = Object.create(null)

    // Store the origin module object which passed by programmer
    this._rawModule = rawModule
    const rawState = rawModule.state

    // Store the origin module's state
    this.state =
      (typeof rawState === "function"
        ? (rawState as () => ModuleState)()
        : // tslint:disable-next-line:no-object-literal-type-assertion
          rawState) || ({} as ModuleState)
  }

  get namespaced() {
    return !!this._rawModule.namespaced
  }

  public addChild<ChildState>(
    key: string,
    module: Module<ChildState, RootState>
  ) {
    this._children[key] = module
  }

  public removeChild(key: string) {
    delete this._children[key]
  }

  public getChild(key: string) {
    return this._children[key]
  }

  public update(rawModule: RawModule<ModuleState, RootState>) {
    this._rawModule.namespaced = rawModule.namespaced
    if (rawModule.actions) {
      this._rawModule.actions = rawModule.actions
    }
    if (rawModule.mutations) {
      this._rawModule.mutations = rawModule.mutations
    }
    if (rawModule.getters) {
      this._rawModule.getters = rawModule.getters
    }
  }

  public forEachChild(fn: any) {
    forEachValue(this._children, fn)
  }

  public forEachGetter(fn: any) {
    if (this._rawModule.getters) {
      forEachValue(this._rawModule.getters, fn)
    }
  }

  public forEachAction(fn: any) {
    if (this._rawModule.actions) {
      forEachValue(this._rawModule.actions, fn)
    }
  }

  public forEachMutation(fn: any) {
    if (this._rawModule.mutations) {
      forEachValue(this._rawModule.mutations, fn)
    }
  }
}

export interface ModuleOptions {
  preserveState?: boolean
}

export interface ModuleTree<R> {
  [key: string]: Module<any, R>
}

export interface RawModule<ModuleState, RootState> {
  actions?: ActionTree<ModuleState, RootState>
  getters?: GetterTree<ModuleState, RootState>
  modules?: ModuleTree<RootState>
  mutations?: MutationTree<ModuleState>
  namespaced?: boolean
  state?: ModuleState | (() => ModuleState)
}
