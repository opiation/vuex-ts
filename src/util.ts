/**
 * Get the first item that pass the test
 * by second argument function
 *
 * @param list
 * @param f
 * @template T
 */
export function find<T = any>(
  list: T[],
  f: (element: T, index: number, array: T[]) => boolean
) {
  return list.filter(f)[0]
}

/**
 * Deep copy the given object considering circular structure.
 * This function caches all nested objects and its copies.
 * If it detects circular structure, use cached copy to avoid infinite loop.
 *
 * @param obj
 * @param cache
 * @template T
 */
export function deepCopy<T = any>(obj: any, cache: any[] = []): T {
  // just return if obj is immutable value
  if (obj === null || typeof obj !== "object") {
    return obj
  }

  // if obj is hit, it is in circular structure
  const hit = find(cache, c => c.original === obj)
  if (hit) {
    return hit.copy
  }

  const copy = Array.isArray(obj) ? [] : ({} as any)

  // put the copy into cache at first
  // because we want to refer it in recursive deepCopy
  cache.push({
    copy,
    original: obj
  })

  Object.keys(obj).forEach(key => {
    copy[key] = deepCopy(obj[key], cache)
  })

  return copy
}

/**
 * forEach for object
 */
export function forEachValue(obj: any, fn: (val: any, key: string) => any) {
  Object.keys(obj).forEach(key => fn(obj[key], key))
}

export function isObject(obj: any) {
  return obj !== null && typeof obj === "object"
}

export function isPromise<T = any>(val: any): val is Promise<T> {
  return val && typeof val.then === "function"
}

export function assert(condition: any, msg: string) {
  if (!condition) {
    throw new Error(`[vuex] ${msg}`)
  }
}
