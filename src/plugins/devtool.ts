import { Store } from "../store"

interface DevTools {
  /**
   * @todo Should use a narrower type here.  Consider getting types used in Vue
   *   DevTools, if there are any
   */
  emit: any

  /**
   * @todo Should use a narrower type here.  Consider getting types used in Vue
   *   DevTools, if there are any
   */
  on: any
}

declare global {
  interface Window {
    __VUE_DEVTOOLS_GLOBAL_HOOK__: DevTools
  }

  // tslint:disable-next-line
  namespace NodeJS {
    interface Global {
      __VUE_DEVTOOLS_GLOBAL_HOOK__: DevTools
    }
  }
}

const fallback: { __VUE_DEVTOOLS_GLOBAL_HOOK__?: DevTools } = {
  __VUE_DEVTOOLS_GLOBAL_HOOK__: undefined
}

const target =
  typeof window !== "undefined"
    ? window
    : typeof global !== "undefined"
    ? global
    : fallback

const devtoolHook = target.__VUE_DEVTOOLS_GLOBAL_HOOK__

export const devtoolPlugin = <T = any>(store: Store<T>) => {
  if (!devtoolHook) {
    return
  }

  store._devtoolHook = devtoolHook

  devtoolHook.emit("vuex:init", store)

  devtoolHook.on("vuex:travel-to-state", (targetState: any) => {
    store.replaceState(targetState)
  })

  store.subscribe((mutation: any, state: any) => {
    devtoolHook.emit("vuex:mutation", mutation, state)
  })
}

export default devtoolPlugin
