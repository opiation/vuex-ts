import Vue from "vue";
export * from "./actions";
export * from "./getters";
export * from "./helpers";
export * from "./mixin";
export * from "./module/module";
export * from "./module/module-collection";
export * from "./mutations";
export * from "./payloads";
export * from "./store";
declare global {
    interface Window {
        Vue: Vue;
    }
}
export declare const version = "__VERSION__";
