import Vue, { VueConstructor } from "vue";
export declare const mixin: (V: VueConstructor<Vue>) => void;
