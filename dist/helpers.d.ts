import Vue from "vue";
import { Dispatch } from "./actions";
import { Commit } from "./mutations";
interface Dictionary<T> {
    [key: string]: T;
}
declare type Computed = () => any;
declare type MutationMethod = (...args: any[]) => void;
declare type ActionMethod = (...args: any[]) => Promise<any>;
declare type CustomVue = Vue & Dictionary<any>;
declare type Mapper<R> = (map: string[] | Dictionary<string>) => Dictionary<R>;
declare type MapperWithNamespace<R> = (namespace: string, map: string[] | Dictionary<string>) => Dictionary<R>;
declare type FunctionMapper<F, R> = (map: Dictionary<(this: CustomVue, fn: F, ...args: any[]) => any>) => Dictionary<R>;
declare type FunctionMapperWithNamespace<F, R> = (namespace: string, map: Dictionary<(this: CustomVue, fn: F, ...args: any[]) => any>) => Dictionary<R>;
declare type MapperForState = <S>(map: Dictionary<(this: CustomVue, state: S, getters: any) => any>) => Dictionary<Computed>;
declare type MapperForStateWithNamespace = <S>(namespace: string, map: Dictionary<(this: CustomVue, state: S, getters: any) => any>) => Dictionary<Computed>;
declare type StateMapper = Mapper<Computed> & MapperWithNamespace<Computed> & MapperForState & MapperForStateWithNamespace;
/**
 * Reduce the code which written in Vue.js for getting the state.
 * @param [namespace] Module's namespace
 * @param states Object's item can be a function which accept state and getters
 *   for param, you can do something for state and getters in it.
 */
export declare const mapState: StateMapper;
declare type MutationMapper = Mapper<MutationMethod> & MapperWithNamespace<MutationMethod> & FunctionMapper<Commit, MutationMethod> & FunctionMapperWithNamespace<Commit, MutationMethod>;
/**
 * Reduce the code which written in Vue.js for committing the mutation
 * @param [namespace] Module's namespace
 * @param mutations Object's item can be a function which accept `commit`
 *   function as the first param, it can accept anthor params.  You can commit
 *   mutation and do any other things in this function.  Specially, you need to
 *   pass anthor params from the mapped function.
 * @return {Object}
 */
export declare const mapMutations: MutationMapper;
declare type GetterMapper = Mapper<Computed> & MapperWithNamespace<Computed>;
/**
 * Reduce the code which written in Vue.js for getting the getters
 * @param [namespace] Module's namespace
 * @param getters
 */
export declare const mapGetters: GetterMapper;
declare type ActionMapper = Mapper<ActionMethod> & MapperWithNamespace<ActionMethod> & FunctionMapper<Dispatch, ActionMethod> & FunctionMapperWithNamespace<Dispatch, ActionMethod>;
/**
 * Reduce the code which written in Vue.js for dispatch the action
 * @param [namespace] Module's namespace
 * @param actions Object's item can be a function which accept `dispatch`
 *   function as the first param, it can accept anthor params. You can dispatch
 *   action and do any other things in this function. Specially, tou need to
 *   pass anthor params from the mapped function.
 * @return
 */
export declare const mapActions: ActionMapper;
/**
 * Rebinding namespace param for mapXXX function in special scoped, and return
 * them by simple object
 * @param namespace
 * @todo Should return object of type `NamespacedMappers`
 */
export declare const createNamespacedHelpers: (namespace: string) => {
    mapActions: (map: Dictionary<(this: CustomVue, fn: Dispatch, ...args: any[]) => any>) => Dictionary<ActionMethod>;
    mapGetters: (map: Dictionary<string> | string[]) => Dictionary<Computed>;
    mapMutations: (map: Dictionary<(this: CustomVue, fn: Commit, ...args: any[]) => any>) => Dictionary<MutationMethod>;
    mapState: (map: Dictionary<(this: CustomVue, state: {}, getters: any) => any>) => Dictionary<Computed>;
};
export {};
