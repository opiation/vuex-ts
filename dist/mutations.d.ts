import { Payload } from "./payloads";
export interface Commit {
    (type: string, payload?: any, options?: CommitOptions): void;
    <P extends Payload>(payloadWithType: P, options?: CommitOptions): void;
}
export declare type Mutation<S> = (state: S, payload: any) => any;
export interface MutationPayload extends Payload {
    payload: any;
}
export interface MutationTree<S> {
    [key: string]: Mutation<S>;
}
export interface CommitOptions {
    silent?: boolean;
    root?: boolean;
}
