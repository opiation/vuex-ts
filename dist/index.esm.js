/**
 * Reduce the code which written in Vue.js for getting the state.
 * @param [namespace] Module's namespace
 * @param states Object's item can be a function which accept state and getters
 *   for param, you can do something for state and getters in it.
 */
const mapState = normalizeNamespace((namespace, states) => {
    const res = {};
    normalizeMap(states).forEach(({ key, val }) => {
        res[key] = function mappedState() {
            let state = this.$store.state;
            let getters = this.$store.getters;
            if (namespace) {
                const module = getModuleByNamespace(this.$store, "mapState", namespace);
                if (!module) {
                    return;
                }
                state = module.context.state;
                getters = module.context.getters;
            }
            return typeof val === "function"
                ? val.call(this, state, getters)
                : state[val];
        };
        // mark vuex getter for devtools
        res[key].vuex = true;
    });
    return res;
});
/**
 * Reduce the code which written in Vue.js for committing the mutation
 * @param [namespace] Module's namespace
 * @param mutations Object's item can be a function which accept `commit`
 *   function as the first param, it can accept anthor params.  You can commit
 *   mutation and do any other things in this function.  Specially, you need to
 *   pass anthor params from the mapped function.
 * @return {Object}
 */
const mapMutations = normalizeNamespace((namespace, mutations) => {
    const res = {};
    normalizeMap(mutations).forEach(({ key, val }) => {
        res[key] = function mappedMutation(...args) {
            // Get the commit method from store
            let commit = this.$store.commit;
            if (namespace) {
                const module = getModuleByNamespace(this.$store, "mapMutations", namespace);
                if (!module) {
                    return;
                }
                commit = module.context.commit;
            }
            return typeof val === "function"
                ? val.apply(this, [commit].concat(args))
                : commit.apply(this.$store, [val].concat(args));
        };
    });
    return res;
});
/**
 * Reduce the code which written in Vue.js for getting the getters
 * @param [namespace] Module's namespace
 * @param getters
 */
const mapGetters = normalizeNamespace((namespace, getters) => {
    const res = {};
    normalizeMap(getters).forEach(({ key, val }) => {
        // The namespace has been mutated by normalizeNamespace
        val = namespace + val;
        res[key] = function mappedGetter() {
            if (namespace &&
                !getModuleByNamespace(this.$store, "mapGetters", namespace)) {
                return;
            }
            if (process.env.NODE_ENV !== "production" &&
                !(val in this.$store.getters)) {
                console.error(`[vuex] unknown getter: ${val}`);
                return;
            }
            return this.$store.getters[val];
        };
        // mark vuex getter for devtools
        res[key].vuex = true;
    });
    return res;
});
/**
 * Reduce the code which written in Vue.js for dispatch the action
 * @param [namespace] Module's namespace
 * @param actions Object's item can be a function which accept `dispatch`
 *   function as the first param, it can accept anthor params. You can dispatch
 *   action and do any other things in this function. Specially, tou need to
 *   pass anthor params from the mapped function.
 * @return
 */
const mapActions = normalizeNamespace((namespace, actions) => {
    const res = {};
    normalizeMap(actions).forEach(({ key, val }) => {
        res[key] = function mappedAction(...args) {
            // get dispatch function from store
            let dispatch = this.$store.dispatch;
            if (namespace) {
                const module = getModuleByNamespace(this.$store, "mapActions", namespace);
                if (!module) {
                    return;
                }
                dispatch = module.context.dispatch;
            }
            return typeof val === "function"
                ? val.apply(this, [dispatch].concat(args))
                : dispatch.apply(this.$store, [val].concat(args));
        };
    });
    return res;
});
/**
 * Rebinding namespace param for mapXXX function in special scoped, and return
 * them by simple object
 * @param namespace
 * @todo Should return object of type `NamespacedMappers`
 */
const createNamespacedHelpers = (namespace) => ({
    mapActions: mapActions.bind(null, namespace),
    mapGetters: mapGetters.bind(null, namespace),
    mapMutations: mapMutations.bind(null, namespace),
    mapState: mapState.bind(null, namespace)
});
function normalizeMap(map) {
    if (Array.isArray(map)) {
        return map.map(key => ({ key, val: key }));
    }
    const keys = Object.keys(map);
    return keys.map(key => ({ key, val: map[key] }));
}
/**
 * Return a function expect two param contains namespace and map.  It will
 * normalize the namespace and then the param's function will handle the new
 * namespace and the map.
 * @param fn
 */
function normalizeNamespace(fn) {
    function normalized(namespace, map) {
        if (typeof namespace !== "string") {
            map = namespace;
            namespace = "";
        }
        else if (namespace.charAt(namespace.length - 1) !== "/") {
            namespace += "/";
        }
        if (map === undefined) {
            throw new TypeError(`map must be a valid type`);
        }
        return fn(namespace, map);
    }
    return normalized;
}
/**
 * Search a special module from store by namespace. if module not exist, print
 * error message.
 * @param store
 * @param helper
 * @param namespace
 */
function getModuleByNamespace(store, helper, namespace) {
    const module = store._modulesNamespaceMap[namespace];
    if (process.env.NODE_ENV !== "production" && !module) {
        console.error(`[vuex] module namespace not found in ${helper}(): ${namespace}`);
    }
    return module;
}

const mixin = (V) => {
    const version = Number(V.version.split(".")[0]);
    if (version >= 2) {
        V.mixin({ beforeCreate: vuexInit });
    }
    else {
        throw new Error("Unsupported version of Vue. Must use >= 2.0");
    }
    /**
     * Vuex init hook, injected into each instances init hooks list.
     */
    function vuexInit() {
        const options = this.$options;
        // store injection
        if (options.store) {
            this.$store =
                typeof options.store === "function"
                    ? options.store()
                    : options.store;
        }
        else if (options.parent && options.parent.$store) {
            this.$store = options.parent.$store;
        }
    }
};

/**
 * Get the first item that pass the test
 * by second argument function
 *
 * @param list
 * @param f
 * @template T
 */
/**
 * forEach for object
 */
function forEachValue(obj, fn) {
    Object.keys(obj).forEach(key => fn(obj[key], key));
}
function isObject(obj) {
    return obj !== null && typeof obj === "object";
}
function isPromise(val) {
    return val && typeof val.then === "function";
}
function assert(condition, msg) {
    if (!condition) {
        throw new Error(`[vuex] ${msg}`);
    }
}

// Base data struct for store's module, package with some attribute and method
class Module {
    // tslint:enable:variable-name
    constructor(rawModule, runtime) {
        this.runtime = runtime;
        // Store some children item
        this._children = Object.create(null);
        // Store the origin module object which passed by programmer
        this._rawModule = rawModule;
        const rawState = rawModule.state;
        // Store the origin module's state
        this.state =
            (typeof rawState === "function"
                ? rawState()
                : // tslint:disable-next-line:no-object-literal-type-assertion
                    rawState) || {};
    }
    get namespaced() {
        return !!this._rawModule.namespaced;
    }
    addChild(key, module) {
        this._children[key] = module;
    }
    removeChild(key) {
        delete this._children[key];
    }
    getChild(key) {
        return this._children[key];
    }
    update(rawModule) {
        this._rawModule.namespaced = rawModule.namespaced;
        if (rawModule.actions) {
            this._rawModule.actions = rawModule.actions;
        }
        if (rawModule.mutations) {
            this._rawModule.mutations = rawModule.mutations;
        }
        if (rawModule.getters) {
            this._rawModule.getters = rawModule.getters;
        }
    }
    forEachChild(fn) {
        forEachValue(this._children, fn);
    }
    forEachGetter(fn) {
        if (this._rawModule.getters) {
            forEachValue(this._rawModule.getters, fn);
        }
    }
    forEachAction(fn) {
        if (this._rawModule.actions) {
            forEachValue(this._rawModule.actions, fn);
        }
    }
    forEachMutation(fn) {
        if (this._rawModule.mutations) {
            forEachValue(this._rawModule.mutations, fn);
        }
    }
}

class ModuleCollection {
    constructor(rawRootModule) {
        // register root module (Vuex.Store options)
        if (process.env.NODE_ENV !== "production") {
            assertRawModule([], rawRootModule);
        }
        const newModule = new Module(rawRootModule, false);
        this.root = newModule;
    }
    get(path) {
        return path.reduce((module, key) => {
            return module.getChild(key);
        }, this.root);
    }
    getNamespace(path) {
        let module = this.root;
        return path.reduce((namespace, key) => {
            module = module.getChild(key);
            return namespace + (module.namespaced ? `${key}/` : "");
        }, "");
    }
    update(rawRootModule) {
        update([], this.root, rawRootModule);
    }
    register(path, rawModule, runtime = true) {
        if (process.env.NODE_ENV !== "production") {
            assertRawModule(path, rawModule);
        }
        const newModule = new Module(rawModule, runtime);
        if (path.length === 0) {
            // If path is [], newModule is the root module of type Module<R, R>
            this.root = newModule;
        }
        else {
            const parent = this.get(path.slice(0, -1));
            parent.addChild(path[path.length - 1], newModule);
        }
        // register nested modules
        if (rawModule.modules) {
            forEachValue(rawModule.modules, (rawChildModule, key) => {
                this.register(path.concat(key), rawChildModule, runtime);
            });
        }
    }
    unregister(path) {
        const parent = this.get(path.slice(0, -1));
        const key = path[path.length - 1];
        if (!parent.getChild(key).runtime) {
            return;
        }
        parent.removeChild(key);
    }
}
function update(path, targetModule, newModule) {
    if (process.env.NODE_ENV !== "production") {
        assertRawModule(path, newModule);
    }
    // update target module
    targetModule.update(newModule);
    // update nested modules
    if (newModule.modules) {
        for (const key in newModule.modules) {
            if (newModule.modules.hasOwnProperty(key)) {
                if (!targetModule.getChild(key)) {
                    if (process.env.NODE_ENV !== "production") {
                        console.warn(`[vuex] trying to add a new module '${key}' on hot reloading, ` +
                            "manual reload is needed");
                    }
                    return;
                }
                update(path.concat(key), targetModule.getChild(key), newModule.modules[key]);
            }
        }
    }
}
const functionAssert = {
    assert: (value) => typeof value === "function",
    expected: "function"
};
const objectAssert = {
    assert: (value) => typeof value === "function" ||
        (typeof value === "object" && typeof value.handler === "function"),
    expected: 'function or object with "handler" function'
};
const assertTypes = {
    actions: objectAssert,
    getters: functionAssert,
    mutations: functionAssert
};
function assertRawModule(path, rawModule) {
    const keys = Object.keys(assertTypes);
    keys.forEach(key => {
        if (!rawModule[key]) {
            return;
        }
        const assertOptions = assertTypes[key];
        forEachValue(rawModule[key], (value, type) => {
            assert(assertOptions.assert(value), makeAssertionMessage(path, key, type, value, assertOptions.expected));
        });
    });
}
function makeAssertionMessage(path, key, type, value, expected) {
    let buf = `${key} should be ${expected} but "${key}.${type}"`;
    if (path.length > 0) {
        buf += ` in module "${path.join(".")}"`;
    }
    buf += ` is ${JSON.stringify(value)}.`;
    return buf;
}

const fallback = {
    __VUE_DEVTOOLS_GLOBAL_HOOK__: undefined
};
const target = typeof window !== "undefined"
    ? window
    : typeof global !== "undefined"
        ? global
        : fallback;
const devtoolHook = target.__VUE_DEVTOOLS_GLOBAL_HOOK__;
const devtoolPlugin = (store) => {
    if (!devtoolHook) {
        return;
    }
    store._devtoolHook = devtoolHook;
    devtoolHook.emit("vuex:init", store);
    devtoolHook.on("vuex:travel-to-state", (targetState) => {
        store.replaceState(targetState);
    });
    store.subscribe((mutation, state) => {
        devtoolHook.emit("vuex:mutation", mutation, state);
    });
};

let Vue; // bind on install
// tslint:disable:variable-name
class Store {
    constructor(options = {}) {
        // Auto install if it is not done yet and `window` has `Vue`.
        // To allow users to avoid auto-installation in some cases,
        // this code should be placed here. See #731
        if (!Vue && typeof window !== "undefined" && window.Vue) {
            install(window.Vue);
        }
        if (process.env.NODE_ENV !== "production") {
            assert(Vue, `must call Vue.use(Vuex) before creating a store instance.`);
            assert(typeof Promise !== "undefined", `vuex requires a Promise polyfill in this browser.`);
            assert(this instanceof Store, `store must be called with the new operator.`);
        }
        const { plugins = [], strict = false } = options;
        // store internal state
        this._committing = false;
        this._actions = Object.create(null);
        this._actionSubscribers = [];
        this._mutations = Object.create(null);
        this._wrappedGetters = Object.create(null);
        this._modules = new ModuleCollection(options);
        this._modulesNamespaceMap = Object.create(null);
        this._subscribers = [];
        this._watcherVM = new Vue();
        // bind commit and dispatch to self
        // tslint:disable-next-line:no-this-assignment
        const store = this;
        // tslint:disable-next-line:no-this-assignment
        const { dispatch, commit } = this;
        this.dispatch = function boundDispatch(type, payload) {
            return dispatch.call(store, type, payload);
        };
        this.commit = function boundCommit(type, payload, commitOptions) {
            return commit.call(store, type, payload, commitOptions);
        };
        // strict mode
        this.strict = strict;
        const state = this._modules.root.state;
        // init root module.
        // this also recursively registers all sub-modules
        // and collects all module getters inside this._wrappedGetters
        installModule(this, state, [], this._modules.root);
        // initialize the store vm, which is responsible for the reactivity
        // (also registers _wrappedGetters as computed properties)
        resetStoreVM(this, state);
        // apply plugins
        plugins.forEach((plugin) => plugin(this));
        const useDevtools = options.devtools !== undefined ? options.devtools : Vue.config.devtools;
        if (useDevtools) {
            devtoolPlugin(this);
        }
    }
    get state() {
        return this._vm._data.$$state;
    }
    set state(v) {
        if (process.env.NODE_ENV !== "production") {
            assert(false, `use store.replaceState() to explicit replace store state.`);
        }
    }
    commit(_type, _payload, _options) {
        // check object-style commit
        const { type, payload, options } = unifyObjectStyle(_type, _payload, _options);
        const mutation = { type, payload };
        const entry = this._mutations[type];
        if (!entry) {
            if (process.env.NODE_ENV !== "production") {
                console.error(`[vuex] unknown mutation type: ${type}`);
            }
            return;
        }
        this._withCommit(() => {
            entry.forEach(function commitIterator(handler) {
                handler(payload);
            });
        });
        this._subscribers.forEach((sub) => sub(mutation, this.state));
        if (process.env.NODE_ENV !== "production" && options && options.silent) {
            console.warn(`[vuex] mutation type: ${type}. Silent option has been removed. ` +
                "Use the filter functionality in the vue-devtools");
        }
    }
    dispatch(_type, _payload) {
        // check object-style dispatch
        const { type, payload } = unifyObjectStyle(_type, _payload);
        const action = { type, payload };
        const entry = this._actions[type];
        if (!entry) {
            if (process.env.NODE_ENV !== "production") {
                console.error(`[vuex] unknown action type: ${type}`);
            }
            return Promise.reject();
        }
        try {
            this._actionSubscribers
                .filter((sub) => sub.before)
                .forEach((sub) => sub.before(action, this.state));
        }
        catch (e) {
            if (process.env.NODE_ENV !== "production") {
                console.warn(`[vuex] error in before action subscribers: `);
                console.error(e);
            }
        }
        const result = entry.length > 1
            ? Promise.all(entry.map((handler) => handler(payload)))
            : entry[0](payload);
        return result.then((res) => {
            try {
                this._actionSubscribers
                    .filter((sub) => sub.after)
                    .forEach((sub) => sub.after(action, this.state));
            }
            catch (e) {
                if (process.env.NODE_ENV !== "production") {
                    console.warn(`[vuex] error in after action subscribers: `);
                    console.error(e);
                }
            }
            return res;
        });
    }
    subscribe(fn) {
        return genericSubscribe(fn, this._subscribers);
    }
    subscribeAction(fn) {
        const subs = typeof fn === "function" ? { before: fn } : fn;
        return genericSubscribe(subs, this._actionSubscribers);
    }
    watch(getter, cb, options) {
        if (process.env.NODE_ENV !== "production") {
            assert(typeof getter === "function", `store.watch only accepts a function.`);
        }
        return this._watcherVM.$watch(() => getter(this.state, this.getters), cb, options);
    }
    replaceState(state) {
        this._withCommit(() => {
            this._vm._data.$$state = state;
        });
    }
    registerModule(path, rawModule, options = {}) {
        if (typeof path === "string") {
            path = [path];
        }
        if (process.env.NODE_ENV !== "production") {
            assert(Array.isArray(path), `module path must be a string or an Array.`);
            assert(path.length > 0, "cannot register the root module by using registerModule.");
        }
        this._modules.register(path, rawModule);
        installModule(this, this.state, path, this._modules.get(path), options.preserveState);
        // reset store to update getters...
        resetStoreVM(this, this.state);
    }
    unregisterModule(path) {
        const normalizedPath = typeof path === "string" ? [path] : path;
        if (process.env.NODE_ENV !== "production") {
            assert(Array.isArray(normalizedPath), `module path must be a string or an Array.`);
        }
        this._modules.unregister(normalizedPath);
        this._withCommit(() => {
            const parentState = getNestedState(this.state, normalizedPath.slice(0, -1));
            Vue.delete(parentState, normalizedPath[normalizedPath.length - 1]);
        });
        resetStore(this);
    }
    hotUpdate(newOptions) {
        this._modules.update(newOptions);
        resetStore(this, true);
    }
    _withCommit(fn) {
        const committing = this._committing;
        this._committing = true;
        fn();
        this._committing = committing;
    }
}
function genericSubscribe(fn, subs) {
    if (subs.indexOf(fn) < 0) {
        subs.push(fn);
    }
    return () => {
        const i = subs.indexOf(fn);
        if (i > -1) {
            subs.splice(i, 1);
        }
    };
}
function resetStore(store, hot = false) {
    store._actions = Object.create(null);
    store._mutations = Object.create(null);
    store._wrappedGetters = Object.create(null);
    store._modulesNamespaceMap = Object.create(null);
    const state = store.state;
    // init all modules
    installModule(store, state, [], store._modules.root, true);
    // reset vm
    resetStoreVM(store, state, hot);
}
function resetStoreVM(store, state, hot = false) {
    const oldVm = store._vm;
    // bind store public getters
    store.getters = {};
    const wrappedGetters = store._wrappedGetters;
    const computed = {};
    forEachValue(wrappedGetters, (fn, key) => {
        // use computed to leverage its lazy-caching mechanism
        computed[key] = () => fn(store);
        Object.defineProperty(store.getters, key, {
            enumerable: true,
            get: () => store._vm[key]
        });
    });
    // use a Vue instance to store the state tree
    // suppress warnings just in case the user has added
    // some funky global mixins
    const silent = Vue.config.silent;
    Vue.config.silent = true;
    store._vm = new Vue({
        computed,
        data: {
            $$state: state
        }
    });
    Vue.config.silent = silent;
    // enable strict mode for new vm
    if (store.strict) {
        enableStrictMode(store);
    }
    if (oldVm) {
        if (hot) {
            // dispatch changes in all subscribed watchers
            // to force getter re-evaluation for hot reloading.
            store._withCommit(() => {
                oldVm._data.$$state = null;
            });
        }
        Vue.nextTick(() => oldVm.$destroy());
    }
}
function installModule(store, rootState, path, module, hot = false) {
    const isRoot = !path.length;
    const namespace = store._modules.getNamespace(path);
    // register in namespace map
    if (module.namespaced) {
        store._modulesNamespaceMap[namespace] = module;
    }
    // set state
    if (!isRoot && !hot) {
        const parentState = getNestedState(rootState, path.slice(0, -1));
        const moduleName = path[path.length - 1];
        store._withCommit(() => {
            Vue.set(parentState, moduleName, module.state);
        });
    }
    const local = (module.context = makeLocalContext(store, namespace, path));
    module.forEachMutation((mutation, key) => {
        const namespacedType = namespace + key;
        registerMutation(store, namespacedType, mutation, local);
    });
    module.forEachAction((action, key) => {
        const type = action.root ? key : namespace + key;
        const handler = action.handler || action;
        registerAction(store, type, handler, local);
    });
    module.forEachGetter((getter, key) => {
        const namespacedType = namespace + key;
        registerGetter(store, namespacedType, getter, local);
    });
    module.forEachChild((child, key) => {
        installModule(store, rootState, path.concat(key), child, hot);
    });
}
/**
 * make localized dispatch, commit, getters and state
 * if there is no namespace, just use root ones
 */
function makeLocalContext(store, namespace, path) {
    const noNamespace = namespace === "";
    const local = {
        dispatch: noNamespace
            ? store.dispatch
            : (_type, _payload, _options) => {
                const args = unifyObjectStyle(_type, _payload, _options);
                const { payload, options } = args;
                let { type } = args;
                if (!options || !options.root) {
                    type = namespace + type;
                    if (process.env.NODE_ENV !== "production" &&
                        !store._actions[type]) {
                        console.error(`[vuex] unknown local action type: ${args.type}, global type: ${type}`);
                        return;
                    }
                }
                return store.dispatch(type, payload);
            },
        commit: noNamespace
            ? store.commit
            : (_type, _payload, _options) => {
                const args = unifyObjectStyle(_type, _payload, _options);
                const { payload, options } = args;
                let { type } = args;
                if (!options || !options.root) {
                    type = namespace + type;
                    if (process.env.NODE_ENV !== "production" &&
                        !store._mutations[type]) {
                        console.error(`[vuex] unknown local mutation type: ${args.type}, global type: ${type}`);
                        return;
                    }
                }
                store.commit(type, payload, options);
            }
    };
    // getters and state object must be gotten lazily
    // because they will be changed by vm update
    Object.defineProperties(local, {
        getters: {
            get: noNamespace
                ? () => store.getters
                : () => makeLocalGetters(store, namespace)
        },
        state: {
            get: () => getNestedState(store.state, path)
        }
    });
    return local;
}
// tslint:enable:variable-name
function makeLocalGetters(store, namespace) {
    const gettersProxy = {};
    const splitPos = namespace.length;
    Object.keys(store.getters).forEach(type => {
        // skip if the target getter is not match this namespace
        if (type.slice(0, splitPos) !== namespace) {
            return;
        }
        // extract local getter type
        const localType = type.slice(splitPos);
        // Add a port to the getters proxy.
        // Define as getter property because
        // we do not want to evaluate the getters in this time.
        Object.defineProperty(gettersProxy, localType, {
            enumerable: true,
            get: () => store.getters[type]
        });
    });
    return gettersProxy;
}
function registerMutation(store, type, handler, local) {
    const entry = store._mutations[type] || (store._mutations[type] = []);
    entry.push(function wrappedMutationHandler(payload) {
        handler.call(store, local.state, payload);
    });
}
function registerAction(store, type, handler, local) {
    const entry = store._actions[type] || (store._actions[type] = []);
    entry.push(function wrappedActionHandler(payload, cb) {
        let res = handler.call(store, {
            commit: local.commit,
            dispatch: local.dispatch,
            getters: local.getters,
            rootGetters: store.getters,
            rootState: store.state,
            state: local.state
        }, payload, cb);
        if (!isPromise(res)) {
            res = Promise.resolve(res);
        }
        if (store._devtoolHook) {
            return res.catch((err) => {
                store._devtoolHook.emit("vuex:error", err);
                throw err;
            });
        }
        else {
            return res;
        }
    });
}
function registerGetter(store, type, rawGetter, local) {
    if (store._wrappedGetters[type]) {
        if (process.env.NODE_ENV !== "production") {
            console.error(`[vuex] duplicate getter key: ${type}`);
        }
        return;
    }
    store._wrappedGetters[type] = function wrappedGetter(rootStore) {
        return rawGetter(local.state, // local state
        local.getters, // local getters
        rootStore.state, // root state
        rootStore.getters // root getters
        );
    };
}
function enableStrictMode(store) {
    store._vm.$watch(function () {
        return this._data.$$state;
    }, () => {
        if (process.env.NODE_ENV !== "production") {
            assert(store._committing, `do not mutate vuex store state outside mutation handlers.`);
        }
    }, { deep: true, sync: true });
}
function getNestedState(state, path) {
    return path.length
        ? path.reduce((stateBranchOrLeaf, key) => stateBranchOrLeaf[key], state)
        : state;
}
// TODO: Possible replace type with union of action interface and string
function unifyObjectStyle(type, payload, options) {
    if (isObject(type) && type.type) {
        options = payload;
        payload = type;
        type = type.type;
    }
    if (process.env.NODE_ENV !== "production") {
        assert(typeof type === "string", `expects string as the type, but found ${typeof type}.`);
    }
    return { type, payload, options };
}
// tslint:disable-next-line:variable-name
function install(VueLocal) {
    if (Vue && VueLocal === Vue) {
        if (process.env.NODE_ENV !== "production") {
            console.error("[vuex] already installed. Vue.use(Vuex) should be called only once.");
        }
        return;
    }
    Vue = VueLocal;
    mixin(Vue);
}

const version = "__VERSION__";

export { version, mapState, mapMutations, mapGetters, mapActions, createNamespacedHelpers, mixin, Module, Store, install };
