import { ActionTree } from "../actions";
import { GetterTree } from "../getters";
import { MutationTree } from "../mutations";
export declare class Module<ModuleState, RootState> {
    runtime: boolean;
    state?: ModuleState;
    actions?: ActionTree<ModuleState, RootState>;
    getters?: GetterTree<ModuleState, RootState>;
    mutations?: MutationTree<ModuleState>;
    private _children;
    private _rawModule;
    constructor(rawModule: RawModule<ModuleState, RootState>, runtime: boolean);
    readonly namespaced: boolean;
    addChild<ChildState>(key: string, module: Module<ChildState, RootState>): void;
    removeChild(key: string): void;
    getChild(key: string): Module<any, RootState>;
    update(rawModule: RawModule<ModuleState, RootState>): void;
    forEachChild(fn: any): void;
    forEachGetter(fn: any): void;
    forEachAction(fn: any): void;
    forEachMutation(fn: any): void;
}
export interface ModuleOptions {
    preserveState?: boolean;
}
export interface ModuleTree<R> {
    [key: string]: Module<any, R>;
}
export interface RawModule<ModuleState, RootState> {
    actions?: ActionTree<ModuleState, RootState>;
    getters?: GetterTree<ModuleState, RootState>;
    modules?: ModuleTree<RootState>;
    mutations?: MutationTree<ModuleState>;
    namespaced?: boolean;
    state?: ModuleState | (() => ModuleState);
}
