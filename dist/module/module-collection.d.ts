import { StoreOptions } from "../store";
import { Module, RawModule } from "./module";
export default class ModuleCollection<RootState> {
    root: Module<RootState, RootState>;
    constructor(rawRootModule: StoreOptions<RootState>);
    get(path: string[]): Module<any, RootState>;
    getNamespace(path: string[]): string;
    update(rawRootModule: RawModule<RootState, RootState>): void;
    register<ModuleState>(path: string[], rawModule: RawModule<ModuleState, RootState>, runtime?: boolean): void;
    unregister(path: string[]): void;
}
