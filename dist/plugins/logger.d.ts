import { Store } from "../store";
export default function createLogger({ collapsed, filter, transformer, mutationTransformer, logger }?: {
    collapsed?: boolean | undefined;
    filter?: ((mutation: any, stateBefore: any, stateAfter: any) => boolean) | undefined;
    transformer?: ((state: any) => any) | undefined;
    mutationTransformer?: ((mut: any) => any) | undefined;
    logger?: Console | undefined;
}): <T = any>(store: Store<T>) => void;
