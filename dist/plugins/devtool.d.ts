import { Store } from "../store";
interface DevTools {
    /**
     * @todo Should use a narrower type here.  Consider getting types used in Vue
     *   DevTools, if there are any
     */
    emit: any;
    /**
     * @todo Should use a narrower type here.  Consider getting types used in Vue
     *   DevTools, if there are any
     */
    on: any;
}
declare global {
    interface Window {
        __VUE_DEVTOOLS_GLOBAL_HOOK__: DevTools;
    }
    namespace NodeJS {
        interface Global {
            __VUE_DEVTOOLS_GLOBAL_HOOK__: DevTools;
        }
    }
}
export declare const devtoolPlugin: <T = any>(store: Store<T>) => void;
export default devtoolPlugin;
