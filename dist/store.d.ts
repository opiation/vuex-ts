import _Vue, { VueConstructor, WatchOptions } from "vue";
import { ActionPayload, ActionTree, DispatchOptions, SubscribeActionOptions } from "./actions";
import { GetterTree } from "./getters";
import { Module, ModuleOptions, ModuleTree } from "./module/module";
import ModuleCollection from "./module/module-collection";
import { CommitOptions, MutationPayload, MutationTree } from "./mutations";
declare module "vue/types/options" {
    interface ComponentOptions<V extends _Vue> {
        store?: Store<any>;
    }
}
declare module "vue/types/vue" {
    interface Vue {
        $store: Store<any>;
    }
}
export declare type Plugin<S> = (store: Store<S>) => any;
export declare class Store<RootState> {
    _actions: any;
    _actionSubscribers: any;
    _committing: any;
    _devtoolHook: any;
    _modules: ModuleCollection<any>;
    _modulesNamespaceMap: any;
    _mutations: any;
    _subscribers: any;
    _vm: any;
    _watcherVM: any;
    _wrappedGetters: any;
    getters: any;
    strict: boolean;
    constructor(options?: StoreOptions<RootState>);
    state: RootState;
    commit(_type: string, _payload?: any, _options?: CommitOptions): void;
    dispatch(_type: string, payload?: any, options?: DispatchOptions): Promise<any>;
    subscribe<P extends MutationPayload>(fn: (mutation: P, state: RootState) => any): () => void;
    subscribeAction<P extends ActionPayload>(fn: SubscribeActionOptions<P, RootState>): () => void;
    watch<T>(getter: (state: RootState, getters: any) => T, cb: (value: T, oldValue: T) => void, options?: WatchOptions): any;
    replaceState(state: RootState): void;
    registerModule<ModuleState>(path: string | string[], rawModule: Module<ModuleState, RootState>, options?: ModuleOptions): void;
    unregisterModule(path: string | string[]): void;
    hotUpdate(newOptions: {
        actions?: ActionTree<RootState, RootState>;
        getters?: GetterTree<RootState, RootState>;
        modules?: ModuleTree<RootState>;
        mutations?: MutationTree<RootState>;
    }): void;
    _withCommit(fn: any): void;
}
export interface StoreOptions<S> {
    state?: S | (() => S);
    getters?: GetterTree<S, S>;
    actions?: ActionTree<S, S>;
    mutations?: MutationTree<S>;
    modules?: any;
    plugins?: Array<Plugin<S>>;
    strict?: boolean;
    devtools?: any;
}
export declare function install(VueLocal: VueConstructor<_Vue>): void;
