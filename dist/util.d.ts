/**
 * Get the first item that pass the test
 * by second argument function
 *
 * @param list
 * @param f
 * @template T
 */
export declare function find<T = any>(list: T[], f: (element: T, index: number, array: T[]) => boolean): T;
/**
 * Deep copy the given object considering circular structure.
 * This function caches all nested objects and its copies.
 * If it detects circular structure, use cached copy to avoid infinite loop.
 *
 * @param obj
 * @param cache
 * @template T
 */
export declare function deepCopy<T = any>(obj: any, cache?: any[]): T;
/**
 * forEach for object
 */
export declare function forEachValue(obj: any, fn: (val: any, key: string) => any): void;
export declare function isObject(obj: any): boolean;
export declare function isPromise<T = any>(val: any): val is Promise<T>;
export declare function assert(condition: any, msg: string): void;
